import keras
from keras import Sequential
from keras.layers import Dense, SimpleRNN
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

def generate_recurrent_neural_network(data, output):
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(data)
    prepared_data = tokenizer.texts_to_sequences(data)
    prepared_data = pad_sequences(prepared_data, maxlen=70)
    print(prepared_data[:10])

    prepared_output = tokenizer.texts_to_sequences(output)

    model = Sequential()

    model.add(SimpleRNN(units = 128, input_shape = (prepared_data.shape[0], prepared_data.shape[1]))

    model.add(Dense(70, activation='softmax'))

    model.compile(loss = 'categorical_crossentropy',
                  optimizer = 'adam',
                  metrics = ['accuracy'])

    model.fit(x = prepared_data, y = prepared_output, epochs = 5, validation_split = 0.2)

    return model, tokenizer
