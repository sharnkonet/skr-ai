#%% [Load libraries]
import json
import itertools

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from glob import glob
from datetime import datetime
from rnn_skrai import generate_recurrent_neural_network

#%% [Load Messages]
def parse_participant_messages(directories, user = 'Sharn-konet Reitsma'):
    """ Parses a JSON file for messenger data.

        Parameters:
        -----------
        directories - list
            A list of directories to concatenate together into one dataset

        Returns:
        -----------
        message_df - pandas Dataframe
            A dataframe where each row makes up a particular message.
        
        reacts - pandas Dataframe
            A dataframe where each row makes up a reaction.

        title - string
            The name of the chat. (Mostly for group chats)
        
        participants - list
            A list of all names included in the chat history as strings.
    """

    def load_json(directory):
        with open(directory.replace("\\", "/"), encoding = 'utf-8') as dataFile:
            message_data = json.load(dataFile)
        
        return message_data

    def rename_message_keys(message):
        
        message_info = {*message.keys()} - {'sender_name', 'timestamp_ms', 'type', 'reactions', 'share', 'content', 'users', 'missed', 'call_duration'}

        message['Details'] = None

        # Doesn't make sense to use a for/else statement here. Usually only one type/ no support for multiple
        for key in message_info:
            value = message.pop(key)
            message['Message'] = [data['uri'] if type(value) is list else str([*value.values()][0]) for data in value]
            message['Type'] = key.capitalize()
            break

        else:
            if 'users' in message.keys():
                name = None
                content = message.pop('content')
                ## NEEDS SUPPORT FOR REMOVING MEMBERS FROM THE GROUP (couldn't find example)
                if message['type'] == 'Subscribe':
                    name = content.split('added')[1]
                    name = name.strip()[:-14]
                message['Message'] = content
                message['Type'] = message['type']
                message['Details'] = name
            else:
                try:
                    message['Message'] = message.pop('content').encode('latin-1').decode('utf-8')
                    message['Type'] = 'Message'
                except KeyError:
                    message['Message'] = None
                    message['Type'] = 'Removed Message'

        message['Name'] = message.pop('sender_name')

        message['Date'] = datetime.fromtimestamp(message.pop('timestamp_ms')/1000)
        message['Date'] = message['Date'].replace(hour=0, minute=0, second = 0, microsecond = 0)

        if 'reactions' in message:
            message['Reacts'] = [(react['actor'], react['reaction'].encode('latin-1').decode('utf-8')) for react in message['reactions']]
        else:
            message['Reacts'] = []

        del message['type']

        return message

    message_data = [*map(load_json, directories)]

    message_data = [data['messages'] for data in message_data]

    message_data = [*itertools.chain(*message_data)]

    message_data = [*map(rename_message_keys, message_data)]

    message_df = pd.DataFrame(message_data)

    message_df = message_df.loc[message_df['Name'] == user, :]

    return message_df

def transform_messages(input_message, tokenizer):
    input_message = input_message.lower()
    input_message = [character for character in input_message]
    input_message = tokenizer.texts_to_sequences(input_message)
    input_message = pad_sequences(input_message, maxlen=70)
    return input_message

json_directories = glob("**/messages/inbox/*/*.json", recursive = True)
unique_chatnames = {*map(lambda dir: dir.split("\\")[-2], json_directories)}
json_directories = [[directory for directory in json_directories if name in directory] for name in unique_chatnames]

all_messages = pd.DataFrame()

for directories in json_directories:
    message_df = parse_participant_messages(directories)
    all_messages = all_messages.append(message_df)


all_messages = all_messages.loc[all_messages['Type'] == 'Message', 'Message'].reset_index()['Message']

#%% [Analyse some info]

lengths = [*map(len, all_messages)]
print("The maximum length message you've ever sent is: {}".format(max(lengths)))
print("\nThe message was: \n {}".format(all_messages.loc[lengths.index(max(lengths))]))

lengths.sort()

# From this it seems like 70 characters would be a good amount of characters to use
plt.hist(lengths[:int(len(lengths)*0.95)], bins = len({*lengths[:int(len(lengths)*0.95)]}))
plt.show()

#%% [Parse Messages into Suitable Input Format]

all_messages = list(all_messages)

all_messages = [*map(str.lower, all_messages)]

all_messages = [*map(lambda message: [character for character in message] + ['</SENT>'], all_messages)]

all_messages = [*map(lambda message: [(message[:i], message[i]) for i in range(1, min(len(message), 70))], all_messages)]

all_messages = [*itertools.chain(*all_messages)]

formatted_messages, next_characters = zip(*all_messages)

#%% 

model, tokenizer = generate_recurrent_neural_network(formatted_messages, next_characters)

input_message = 'Help '

while input_message[-1] != '</SENT>' and len(input_message) < 70:
    prepared_input_message = transform_messages(input_message, tokenizer)
    predictions = model.predict(prepared_input_message)
    prediction = np.argmax(predictions)
    input_message = input_message + prediction
   


print(all_messages)
